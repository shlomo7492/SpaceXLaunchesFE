import React from 'react'

import Header from "../components/Header/Header";
import classes from "./MainLayout.module.css";


const mainLayout = (props)=>{
    return(
        <div className={classes.Layout}>
            <Header/>
            <main>{props.children}</main>
        </div>
    );
}

export default mainLayout;