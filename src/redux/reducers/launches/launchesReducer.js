import { fromJS } from "immutable";
import {GET_LAUNCH,GET_LAUNCHES,GET_LAST,SET_PAGE_AND_RESULTS} from "../../actions/launchActions"
import {setLaunches,setLast,setCurrentLaunch,setPageAndResults} from "../../../services/launchesUtilityService"

export const initialState = fromJS({
        launches:[
            {"flight_number": 0,
            "mission_name": "",
            "launch_site": {
              "site_id": "",
              "site_name": "",
              "site_name_long": ""
            }
          }],
        lastLaunchNumber:0,
        currentLaunch:{},
        page:1,
        results:5
    }
)
const launchesReducer = (state=initialState, action) =>{
    switch (action.type){
        case GET_LAUNCH:return setCurrentLaunch(state,action.data)
        case GET_LAST:return setLast(state,action.data)
        case GET_LAUNCHES:return setLaunches(state,action.data)
        case SET_PAGE_AND_RESULTS:return setPageAndResults(state,action.data)
        default: return state;
    }
}
export default launchesReducer;