import LaunchesReducer from "./launches/launchesReducer";

import { combineReducers } from "redux";

//In normal cases there will be more than one reducer
const reducer = combineReducers({
  launchState: LaunchesReducer
});

export default reducer;