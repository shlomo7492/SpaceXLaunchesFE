import {getOneLaunch,getPagedLaunches,getLastLaunchNumber} from "../../services/fetchLaunchesService";

export const GET_LAUNCHES = "GET_LAUNCHES";
export const getAllLaunches =(data)=>{
    return function (dispatch){
        getPagedLaunches(data).then(
            
            res=>{if(res) dispatch({type:GET_LAUNCHES,data:res})}
        )
    }
}

export const GET_LAUNCH = "GET_LAUNCH";
export const getLaunch =(data)=>{
    console.log(data)
    return function (dispatch){
        getOneLaunch(data).then(
            res=>{if(res) dispatch({type:GET_LAUNCH,data:res})}
        )
    }
}
export const GET_LAST = "GET_LAST";
export const getLast =(data)=>{
    return function (dispatch){
        getLastLaunchNumber(data).then(
            res=>{if(res) dispatch({type:GET_LAST,data:res})}
        )
    }
}

export const SET_PAGE_AND_RESULTS = "SET_PAGE_AND_RESULTS";
export const setPageAndResults = (data)=>{
    return({type:SET_PAGE_AND_RESULTS,data:data});
}