//TODO: This will be connected component that displays list of all launches paginated
import React, { useEffect }  from 'react';
import {useSelector,useDispatch} from "react-redux";
import queryString from "query-string";
import {getAllLaunches,getLast,setPageAndResults} from "../../redux/actions/launchActions";

import LaunchesView from "../../components/LaunchesView/LaunchesView";
import ViewsPerPage from "../../components/ViewsPerPage/ViewsPerPage";
import Paginate from "../../components/Pagination/Pagination";

//This array contains the values for result per page functionality
const viewsPerPageArr = [5,10,20,50];

const Launches = (props) =>{
    
    
    const queries = queryString.parse(props.location.search);
    
    //Redux selectors
    const dispatch = useDispatch();
    const launches = useSelector(state=>state.launchState.get("launches"));
    const launchesCount = useSelector(state=>state.launchState.get("lastLaunchNumber"));
    const page = useSelector(state=>state.launchState.get("page"));
    const results = useSelector(state=>state.launchState.get("results"));
    
    useEffect(()=>{
        if(props.location.search) {
            const queryData = {};//Will contain the formatted data for api querying 
            queryData.limit = queries.results_per_page;
            queryData.offset = (queries.page-1)*queryData.limit;
            console.log(queryData,queries);
            //Updates the redux store with current page and results per page values
            dispatch(setPageAndResults({page:queries.page,results:queries.results_per_page}))
            //Get the paged data from api and dispatch the action for updating the redux store
            dispatch(getAllLaunches(queryData));
            //Get the flight_number (closest to id) of the last launch for pagination purpose
            dispatch(getLast())
        }else{
            props.history.push(`/?page=${page}&results_per_page=${results}`)
        }
    },[props.history.location])

    //Methods

    //On click view button handler
    const onViewSingleLaunchHandler =(event)=>{
        event.preventDefault();
        props.history.push(`launch/${event.target.id}`);
    }

    //Handles the the change of results per page
    const onChangeResultsNumberHandler =(event)=>{
        event.preventDefault();
        props.history.push(`/?page=1&results_per_page=${event.target.id}`)
    }
    
    //Pagination handler...
    const onChangePageHandler =(event)=>{
        event.preventDefault();
        props.history.push(`/?page=${event.target.id}&results_per_page=${queries.results_per_page}`)
    }

    return(
        <>
.            <ViewsPerPage current={parseInt(queries.results_per_page)} 
                          viewsPerPage={viewsPerPageArr}
                          onclick={onChangeResultsNumberHandler}/>
            <LaunchesView launches={launches.toJS()} onclick={onViewSingleLaunchHandler}/>
            <Paginate {...props} 
                      totalLaunches={launchesCount}
                      currentPage={parseInt(queries.page)}
                      currentViewsPerPage={parseInt(queries.results_per_page)}
                      onclick={onChangePageHandler}/>
        </>
    )
}

export default Launches;