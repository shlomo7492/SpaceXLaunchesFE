import React, {useEffect} from 'react';
import {useSelector,useDispatch} from "react-redux";
import moment from "moment";
import {getLaunch} from "../../redux/actions/launchActions";
import classes from "./Launch.module.css";
const Launch = (props) =>{

    //Redux selectors
    const dispatch = useDispatch();
    const launch = useSelector(state=>state.launchState.get("currentLaunch"));
    const page = useSelector(state=>state.launchState.get("page"));
    const results = useSelector(state=>state.launchState.get("results"));


    useEffect(() => {
        dispatch(getLaunch(props.match.params))
    }, [])

    //formatting the date and time
    const date = moment(launch.get("launch_date_utc")).format("DD.MM.YYYY hh:mm a");
    
    //Back button handler
    const onBackToListHandler = (event)=>{
        event.preventDefault();
        props.history.push(`/?page=${page}&results_per_page=${results}`)
    }
    return(
        <div className={classes.ViewLaunch}>
            <div><span>Mission name:</span>{launch.get("mission_name")}</div>
            <div><span>When: </span>{date}</div>
            <div><span>Mission details:</span>{launch.get("details")}</div>
            <div className={classes.Button}><button onClick={onBackToListHandler}>Back</button></div>
        </div>
    )
}

export default Launch;