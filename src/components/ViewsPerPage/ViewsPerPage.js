import React from 'react';
import classes from "./ViewsPerPage.module.css";

const viewsPerPage = (props) =>{
    
    return(
        <div className={classes.ViewsPerPageContainer}>
        <div className={classes.Label}>Results per page:</div>
        {props.viewsPerPage.map(number =>{
            const buttonStyle = props.current === number ? classes.Selected:"";
            return <button onClick={props.onclick} key={number} id={number} className={buttonStyle}>{number}</button>
        })}
        </div>
    )
}
export default viewsPerPage;