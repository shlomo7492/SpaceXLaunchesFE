import React from 'react';
import moment from "moment"

const launchRow = (props)=>{
    
    const rowStyle = props.type==="title" 
                    ? props.classes.Row+" "+props.classes.TitleLabel
                    :props.classes.Row;
    //Date and time formatting with moment
    const date = moment(props.launch_date_utc).format("DD.MM.YYYY hh:mm a");
    return (
        
        <div className={rowStyle} >
            {props.type === "title" ?
                <>
                    <div>Flight number</div>
                    <div>Mission name</div>
                    <div>Launch date</div>
                    <div>Launch site</div>
                    <div>&nbsp;</div>

                </>
                :
                <>
                    <div>
                        <span className={props.classes.Label}>Flight number:</span>
                        {props.flight_number}
                    </div>
                    <div>
                        <span className={props.classes.Label}>Mission name:</span>
                        {props.mission_name}
                    </div>
                    <div>
                        <span className={props.classes.Label}>Launch date:</span>
                        {date}
                    </div>
                    <div>
                        <span className={props.classes.Label}>Launch site:</span>
                        {props.launch_site.site_name}
                    </div>
                    <div>
                        <button className={props.classes.ViewButton} 
                                 id={props.flight_number} 
                                 onClick={props.onclick}>View</button>
                    </div>
                </>
            }
        </div>
    )
}

export default launchRow;