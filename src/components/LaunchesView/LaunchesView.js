import React from 'react';
import LaunchRow from "./LaunchRow";
import classes from "./LaunchesView.module.css";

const launchesView = (props) => {
 
    return(
        <div className={classes.LaunchesTable}>
            <LaunchRow type="title" classes={classes}/>
            {
                props.launches.map((launch,index) =>
                        <LaunchRow {...launch} 
                                    key={index} 
                                    classes={classes} 
                                    onclick={props.onclick}/>)
            }
        </div>
    )
}

export default launchesView;