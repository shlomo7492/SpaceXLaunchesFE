import React from 'react';

import Logo from "../Logo/Logo";
import classes from "./Header.module.css";

const header = (props)=>{
    return (
        <header className={classes.Header}>
            <h2>SpaceX Launches Report</h2>
            <Logo/>
        </header>
    )
}
export default header;