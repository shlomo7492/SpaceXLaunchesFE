import React from "react";
import classes from "./Pagination.module.css"
const paginate = (props)=>{
    //Calculate the number of pages
    let pages = parseInt(props.totalLaunches/props.currentViewsPerPage);
    if(props.totalLaunches%props.currentViewsPerPage>0){
        pages +=1;
    }
    //Creating array with clickable pages
    const pagesArray = ['first',]
    if(props.currentPage>2){
        pagesArray.push(props.currentPage-2);
    }
    if(props.currentPage>1){
        pagesArray.push(props.currentPage-1);
    }
    pagesArray.push(props.currentPage);
    if(pages-props.currentPage>=1) {
        pagesArray.push(props.currentPage+1);
    }
    if(pages-props.currentPage>=2) {
        pagesArray.push(props.currentPage+2);
    }
    pagesArray.push("last");
    
    return(
        <div className={classes.Pagination}>
            {pagesArray.map(page=>{
                const pageStyle = page===props.currentPage?classes.PaginationItem+" "+classes.Current:classes.PaginationItem;
                return (<div key={page} 
                             id={page==="first"?1:page==="last"?pages:page}
                             className={pageStyle}
                             onClick={props.onclick}>{page==="first"?"First page":page==="last"?"Last page":page}</div>)
            })}
        </div>
    )
}

export default paginate;