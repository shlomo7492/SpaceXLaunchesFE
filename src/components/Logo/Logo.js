import React from 'react';
import imgLogo from "../../assets/images/SpaceX-Emblem.png"
const logo = ()=>{
    return(
        <img src={imgLogo} width="150px" alt="spaceX" title="SpaceX launches"/>
    )
}

export default logo;