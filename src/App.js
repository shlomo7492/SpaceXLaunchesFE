import {withRouter,Route, Switch} from "react-router-dom";

import MainLayout from "./layout/MainLayout";
import Launches from "./pages/Launches/Launches";
import Launch from "./pages/Launch/Launch";

function App(props) {
  return (
    <MainLayout>
      <Switch>
        <Route path="/" exact component={Launches}/>
        <Route path="/launch/:flight_number" component={Launch}/>
      </Switch>
    </MainLayout>
  );
}

export default withRouter(App);
