const successCodes = [200, 201, 304, 404];

export const  fetchApiCall = async (options)=>{
    return await fetch(options.url,{
                        method:options.method,
                        headers:options.headers})
                .then(res=>{
                    if(successCodes.includes(res.status)){
                        return res.json();
                    }
                })
                .catch(error=>console.log(error))
}