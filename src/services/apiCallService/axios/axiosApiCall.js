import axios from "axios";

const successCodes = [200, 201, 304, 404];
export const  axiosApiCall = async (options)=>{
    return await axios(options.url,{
                        method:options.method,
                        headers:options.headers})
                .then(res=>{
                    if(successCodes.includes(res.status)){
                        return res.data;
                    }
                })
                .catch(error=>console.log(error))
}