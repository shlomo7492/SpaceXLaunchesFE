import {fetchApiCall} from "./fetch/fetchApiCall";

import {axiosApiCall} from "./axios/axiosApiCall";

export const apiCall = async (options)=>{
    const requestObject = {
        ...options,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        }
    }
    if("fetch" in window){
        return await fetchApiCall(requestObject);
    } else {
        return await axiosApiCall(requestObject);
    }
}