import {apiCall} from "./apiCallService/apiCall";

const baseUrl = "http://localhost:3001/launches";

export const getOneLaunch = (data)=>{
    console.log(data)
    const queryString = `?flight_number=${data.flight_number}`
    return apiCall({
        url:baseUrl+queryString,
        method: "GET"
    })
    .then(response => response)
    .catch(error=>console.log("Get launch:",error))
}

export const getPagedLaunches = (data)=>{
    const queryString = `?limit=${data.limit}&offset=${data.offset}&filter=flight_number,mission_name,launch_data_uts,launch_date_utc,launch_site`
    return apiCall({
        url:baseUrl+queryString,
        method: "GET"
    })
    .then(res=>res)
    .catch(error=>console.log("Get launch:",error))
}

export const getLastLaunchNumber = () =>{
    return apiCall({
        url:baseUrl+"/latest?filter=flight_number",
        method: "GET"
    })
    .then(res=>res)
    .catch(error=>console.log("Get launch:",error))
}