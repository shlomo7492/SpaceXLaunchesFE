import { fromJS } from "immutable";

export const setLaunches = (state, actionData) =>{
   return state.setIn(["launches"],fromJS(actionData));
}
export const setCurrentLaunch = (state, actionData) =>{
   return state.setIn(["currentLaunch"],fromJS(actionData[0]));
}
export const setLast = (state, actionData) =>{
   return state.setIn(["lastLaunchNumber"],fromJS(actionData.flight_number));
}
export const setPageAndResults = (state, actionData) =>{
   return state.setIn(["page"],fromJS(actionData.page)).setIn(["results"],fromJS(actionData.results));
}